./RNS -o TOV_rns.h5   -r 0.0015 -t tab -f EOS/SLyPP_table.rns
./RNS -o UNIF_rns.h5  -r 0.0015 -t tab -f EOS/SLyPP_table.rns -u -a 0.95
./RNS -o UNIF1_rns.h5 -r 0.0015 -t tab -f EOS/SLyPP_table.rns -u -a 0.7

./RNS_readID -i TOV_rns.h5   -t tab -f EOS/SLyPP_table.rns     -o TOV_rns_ID.h5
./RNS_readID -i UNIF_rns.h5  -t tab -f EOS/SLyPP_table.rns -u  -o UNIF_rns_ID.h5
./RNS_readID -i UNIF1_rns.h5 -t tab -f EOS/SLyPP_table.rns -u  -o UNIF1_rns_ID.h5


../Applications/RotatingStars/ComputeRotatingStar -i LORENEpar/par_rot.d.tov   -e LORENEpar/par_eos_SLypp_table.d -o resultsTOV.d
../Applications/RotatingStars/ComputeRotatingStar -i LORENEpar/par_rot.d.unif  -e LORENEpar/par_eos_SLypp_table.d -o resultsUNIF.d
../Applications/RotatingStars/ComputeRotatingStar -i LORENEpar/par_rot.d.unif1 -e LORENEpar/par_eos_SLypp_table.d -o resultsUNIF1.d


../Applications/RotatingStars/ReadRotatingStar -i resultsTOV.d   -o resultsTOV.h5 
../Applications/RotatingStars/ReadRotatingStar -i resultsUNIF.d  -o resultsUNIF.h5 
../Applications/RotatingStars/ReadRotatingStar -i resultsUNIF1.d -o resultsUNIF1.h5 




#d=H5.ReadHDF5('TOV_rns_ID.h5')
#d1=H5.ReadHDF5('results.h5')
#plot(d['x[0]'][15,15,:],d['rho'][15,15,:],'r',d1['x[0]'][15,15,:]/CU_to_km,d1['rho'][15,15,:]*1e-3,'b')  
  