
import sys
#sys.path.append('/Users/depietri/SVN/BNS2016/PyCactus')
sys.path.append('/Users/depietri/BitBucketsProjects/pycactus')
sys.path.append('/Users/depietri/BitBucketsProjects/PyCactus')
from LoadAll import *


d1 = H5.ReadHDF5('TOV_rns_ID.h5')
d2 = H5.ReadHDF5('UNIF_rns_ID.h5')
d3 = H5.ReadHDF5('UNIF1_rns_ID.h5')
l1 = H5.ReadHDF5('resultsTOV.h5')
l2 = H5.ReadHDF5('resultsUNIF.h5')
l3 = H5.ReadHDF5('resultsUNIF1.h5')

PP.PostProcessing3dFull(d1)
PP.PostProcessing3dFull(d2)
PP.PostProcessing3dFull(d3)
PP.PostProcessing3dFull(l1)
PP.PostProcessing3dFull(l2)
PP.PostProcessing3dFull(l3)


fig = plt.figure(figsize=(9,6))
ax=dict()
ax['rho']=subplot(3,3,1)
ax['press']=subplot(3,3,2)
ax['eps']=subplot(3,3,3)
ax['vel[1]']=subplot(3,3,4)
ax['betay']=subplot(3,3,5)
ax['Omega']=subplot(3,3,6)
ax['alp']=subplot(3,3,7)
ax['gxx']=subplot(3,3,8)
ax['Kxy']=subplot(3,3,9)

def WRITE(ax,d,ls) :
    for i in list(ax):
        I0=((d[i].shape)[0]-1)/2
        I1=((d[i].shape)[1]-1)/2
        ax[i].plot(d['X[0]'][I0,I1,:],d[i][I0,I1,:],ls)
        ax[i].set_title(i)

def WRITE_DIF(ax,d,l) :
    for i in list(ax):
        I0=((d[i].shape)[0]-1)/2
        I1=((d[i].shape)[1]-1)/2
        ax[i].plot(d['X[0]'][I0,I1,:],(d[i][I0,I1,:]-l[i][I0,I1,:]),'k')
        ax[i].set_title(i)
