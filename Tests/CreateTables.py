import sys
#sys.path.append('/Users/depietri/SVN/BNS2016/PyCactus')
sys.path.append('/Users/depietri/BitBucketsProjects/pycactus')
sys.path.append('/Users/depietri/BitBucketsProjects/PyCactus')
from LoadAll import *
from UtilityEOStable import *

#EXAMPLE (SLy entry): 

ks,rhos,gammas=PPparameters_From_ReadTable(1.685819e2,34.384,3.005,2.988,2.851)
rho,press,epsilon = GenerateTableFromPPparamters(ks[0],rhos,gammas,rhoMIN=1e-16,rhoMAX=1e-2,NP=40)
EOSWriteTable(rho,press,epsilon,fname='EOS/SLyPP_table',description='')
