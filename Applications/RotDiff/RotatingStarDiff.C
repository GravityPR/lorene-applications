/*
 * Methods for class Et_rot_diff.
 *
 * (see file et_rot_diff.h for documentation).
 *
 */

/*
 *   Copyright (c) 2001 Eric Gourgoulhon
 *
 *   This file is part of LORENE.
 *
 *   LORENE is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   LORENE is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with LORENE; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


 

/*
 * $Id: et_rot_diff.C,v 1.5 2016/12/05 16:17:53 j_novak Exp $
 * $Log: et_rot_diff.C,v $
 * Revision 1.5  2016/12/05 16:17:53  j_novak
 * Suppression of some global variables (file names, loch, ...) to prevent redefinitions
 *
 * Revision 1.4  2014/10/13 08:52:57  j_novak
 * Lorene classes and functions now belong to the namespace Lorene.
 *
 * Revision 1.3  2004/03/25 10:29:05  j_novak
 * All LORENE's units are now defined in the namespace Unites (in file unites.h).
 *
 * Revision 1.2  2001/12/04 21:27:53  e_gourgoulhon
 *
 * All writing/reading to a binary file are now performed according to
 * the big endian convention, whatever the system is big endian or
 * small endian, thanks to the functions fwrite_be and fread_be
 *
 * Revision 1.1.1.1  2001/11/20 15:19:28  e_gourgoulhon
 * LORENE
 *
 * Revision 1.3  2001/10/25  09:20:54  eric
 * Ajout de la fonction virtuelle display_poly.
 * Affichage de Max nbar, ener et press.
 *
 * Revision 1.2  2001/10/24  16:23:01  eric
 * *** empty log message ***
 *
 * Revision 1.1  2001/10/19  08:18:10  eric
 * Initial revision
 *
 *
 * $Header: /cvsroot/Lorene/C++/Source/Etoile/et_rot_diff.C,v 1.5 2016/12/05 16:17:53 j_novak Exp $
 *
 */

// Headers C
#include <cmath>
#include <cassert>

// Headers Lorene
#include "RotatingStarDiff.h"
#include "tenseur.h"
#include "eos.h"
#include "nbr_spx.h"
#include "utilitaires.h"
#include "unites.h"    

//--------------//
// Constructors //
//--------------//

// Standard constructor
// --------------------
namespace Lorene {
  RotatingStarDiff::RotatingStarDiff(Map& mpi, int nzet_i, bool relat, const Eos& eos_i,
			   double (*frot_i)(double, const Tbl&), 
			   double (*primfrot_i)(double, const Tbl&), 
			   const Tbl& par_frot_i)
    : Star (mpi, nzet_i, eos_i),
      frot(frot_i),                 //
      primfrot(primfrot_i),         //
      par_frot(par_frot_i),         // original elements of the file
      omega_field(mpi),            //
      prim_field(mpi),             //
      relativistic(relat),          //
      a_car(mpi),
      bbb(mpi),
      b_car(mpi),
      nphi(mpi),
      tnphi(mpi),
      uuu(mpi),
      nuf(mpi),
      nuq(mpi),
      dzeta(mpi),
      tggg(mpi),
      w_shift(mpi, CON, mp.get_bvect_cart()),
      khi_shift(mpi),
      tkij(mpi, COV, mp.get_bvect_cart()),
      ak_car(mpi),
      ssjm1_nuf(mpi),
      ssjm1_nuq(mpi),
      ssjm1_dzeta(mpi),
      ssjm1_tggg(mpi),
      ssjm1_khi(mpi),
      ssjm1_wshift(mpi, CON, mp.get_bvect_cart())
  {
      
    // Parameter 1/c^2 is deduced from relativistic:
    unsurc2 = relativistic ? double(1) : double(0) ;
      
    // To make sure that omega is not used
    omega = __infinity ; 
    
    // Initialization to a static state : 
    omega_field = 0 ; 
    prim_field = 0 ; 
    omega_min = 0 ;
    omega_max = 0 ;
      
    // Initialization to a flat metric :
    a_car = 1 ;
    bbb = 1 ;
    bbb.std_spectral_base() ;
    b_car = 1 ;
    nphi = 0 ;
    tnphi = 0 ;
    nuf = 0 ;
    nuq = 0 ;
    dzeta = 0 ;
    tggg = 0 ;
    
    w_shift.set_etat_zero() ;
    khi_shift =  0 ;
    
    beta.set_etat_zero() ;
    beta.set_triad( mp.get_bvect_cart() ) ;
    
    tkij.set_etat_zero() ;
    
    ak_car = 0 ;
    
    ssjm1_nuf = 0 ;
    ssjm1_nuq = 0 ;
    ssjm1_dzeta = 0 ;
    ssjm1_tggg = 0 ;
    ssjm1_khi = 0 ;
    
    ssjm1_wshift.set_etat_zero() ;
    
    // Pointers of derived quantities initialized to zero :
    set_der_0x0();
    
  }

  // Copy constructor
  // ----------------
  RotatingStarDiff::RotatingStarDiff(const RotatingStarDiff& et)
    : Star(et),
      frot(et.frot),                    //
      primfrot(et.primfrot),            //
      par_frot(et.par_frot),            //      Original members of the file
      omega_field(et.omega_field),      //
      omega_min(et.omega_min),          //
      omega_max(et.omega_max),          //
      prim_field(et.prim_field),        //
      relativistic(et.relativistic),
      unsurc2(et.unsurc2),
      omega(et.omega),
      a_car(et.a_car),
      bbb(et.bbb),
      b_car(et.b_car),
      nphi(et.nphi),
      tnphi(et.tnphi),
      uuu(et.uuu),
      nuf(et.nuf),
      nuq(et.nuq),
      dzeta(et.dzeta),
      tggg(et.tggg),
      w_shift(et.w_shift),
      khi_shift(et.khi_shift),
      tkij(et.tkij),
      ak_car(et.ak_car),
      ssjm1_nuf(et.ssjm1_nuf),
      ssjm1_nuq(et.ssjm1_nuq),
      ssjm1_dzeta(et.ssjm1_dzeta),
      ssjm1_tggg(et.ssjm1_tggg),
      ssjm1_khi(et.ssjm1_khi),
      ssjm1_wshift(et.ssjm1_wshift)
    
  {
      // Pointers of derived quantities initialized to zero :
      set_der_0x0() ;
  }
   

  // Constructor from a file
  // -----------------------
  RotatingStarDiff::RotatingStarDiff(Map& mpi, const Eos& eos_i, FILE* fich,
                                     double (*frot_i)(double, const Tbl&),
                                     double (*primfrot_i)(double, const Tbl&) )
    : Star(mpi, eos_i, fich),   // Constructor form file
      par_frot(fich),           //       "      "    "
      frot(frot_i),             // derived quantities
      primfrot(primfrot_i),     // derived quantities
      omega_field(mpi),         // Create the objects from spectral grid
      prim_field(mpi),          //
      a_car(mpi),
      bbb(mpi),
      b_car(mpi),
      nphi(mpi),
      tnphi(mpi),
      uuu(mpi),
      nuf(mpi),
      nuq(mpi),
      dzeta(mpi),
      tggg(mpi),
      w_shift(mpi, CON, mp.get_bvect_cart()),
      khi_shift(mpi),
      tkij(mpi, COV, mp.get_bvect_cart()),
      ak_car(mpi),
      ssjm1_nuf(mpi),
      ssjm1_nuq(mpi),
      ssjm1_dzeta(mpi),
      ssjm1_tggg(mpi),
      ssjm1_khi(mpi),
      ssjm1_wshift(mpi, CON, mp.get_bvect_cart())
  {
    // Star parameters
    // -----------------
      
    // relativistic is read in the file:
    fread(&relativistic, sizeof(bool), 1, fich) ;	//## to be checked !
    
    // ----------- manca prrot
      
    // Parameter 1/c^2 is deduced from relativistic:
    unsurc2 = relativistic ? double(1) : double(0) ;
    
      
    //original parameters
    Scalar omega_field_file(mp, *(mp.get_mg()), fich) ;
    omega_field = omega_field_file ; 
    fait_prim_field() ; 
     
    // omega_min and omega_max are read in the file:     
    fread_be(&omega_min, sizeof(double), 1, fich) ;
    fread_be(&omega_max, sizeof(double), 1, fich) ;

    // Read of the saved fields:
    // ------------------------
    
    Scalar nuf_file(mp, *(mp.get_mg()), fich) ;
    nuf = nuf_file ;
      
    Scalar nuq_file(mp, *(mp.get_mg()), fich) ;
    nuq = nuq_file ;
    
    Scalar dzeta_file(mp, *(mp.get_mg()), fich) ;
    dzeta = dzeta_file ;
      
    Scalar tggg_file(mp, *(mp.get_mg()), fich) ;
    tggg = tggg_file ;
      
    Vector w_shift_file(mp, mp.get_bvect_cart(), fich) ;
    w_shift = w_shift_file ;
      
    Scalar khi_shift_file(mp, *(mp.get_mg()), fich) ;
    khi_shift = khi_shift_file ;
      
    fait_shift() ;	    // constructs shift from w_shift and khi_shift
    fait_nphi() ;       // constructs N^phi from (N^x,N^y,N^z)
      
    Scalar ssjm1_nuf_file(mp, *(mp.get_mg()), fich) ;
    ssjm1_nuf = ssjm1_nuf_file ;
      
    Scalar ssjm1_nuq_file(mp, *(mp.get_mg()), fich) ;
    ssjm1_nuq = ssjm1_nuq_file ;
      
    Scalar ssjm1_dzeta_file(mp, *(mp.get_mg()), fich) ;
    ssjm1_dzeta = ssjm1_dzeta_file ;
      
    Scalar ssjm1_tggg_file(mp, *(mp.get_mg()), fich) ;
    ssjm1_tggg = ssjm1_tggg_file ;
      
    Scalar ssjm1_khi_file(mp, *(mp.get_mg()), fich) ;
    ssjm1_khi = ssjm1_khi_file ;
      
    Vector ssjm1_wshift_file(mp, mp.get_bvect_cart(), fich) ;
    ssjm1_wshift = ssjm1_wshift_file ;

    // All other fields are initialized to zero :
    // ----------------------------------------
    a_car = 0 ;
    bbb = 0 ;
    b_car = 0 ;
    uuu = 0 ;
    tkij.set_etat_zero() ;
    ak_car = 0 ;
      
    // Pointers of derived quantities initialized to zero
    // --------------------------------------------------
    set_der_0x0() ;
      
  }
    
    
  //------------//
  // Destructor //
  //------------//

  RotatingStarDiff::~RotatingStarDiff(){

    del_deriv() ;
  }
    
  //----------------------------------//
  // Management of derived quantities //
  //----------------------------------//
    

    
void RotatingStarDiff::del_deriv() const {
        
    Star::del_deriv() ;
        
    if (p_angu_mom != 0x0) delete p_angu_mom ;
    if (p_tsw != 0x0) delete p_tsw ;
    if (p_grv2 != 0x0) delete p_grv2 ;
    if (p_grv3 != 0x0) delete p_grv3 ;
    if (p_r_circ != 0x0) delete p_r_circ ;
    if (p_area != 0x0) delete p_area ;
    if (p_aplat != 0x0) delete p_aplat ;
    if (p_z_eqf != 0x0) delete p_z_eqf ;
    if (p_z_eqb != 0x0) delete p_z_eqb ;
    if (p_z_pole != 0x0) delete p_z_pole ;
    if (p_mom_quad != 0x0) delete p_mom_quad ;
    if (p_surf_grav != 0x0) delete p_surf_grav ;
    if (p_r_isco != 0x0) delete p_r_isco ;
    if (p_f_isco != 0x0) delete p_f_isco ;
    if (p_lspec_isco != 0x0) delete p_lspec_isco ;
    if (p_espec_isco != 0x0) delete p_espec_isco ;
    if (p_f_eq != 0x0) delete p_f_eq ;
        
    RotatingStarDiff::set_der_0x0() ;
}
    
void RotatingStarDiff::set_der_0x0() const {
    
    Star::set_der_0x0() ;
        
    p_angu_mom = 0x0 ;
    p_tsw = 0x0 ;
    p_grv2 = 0x0 ;
    p_grv3 = 0x0 ;
    p_r_circ = 0x0 ;
    p_area = 0x0 ;
    p_aplat = 0x0 ;
    p_z_eqf = 0x0 ;
    p_z_eqb = 0x0 ;
    p_z_pole = 0x0 ;
    p_mom_quad = 0x0 ;
    p_surf_grav = 0x0 ;
    p_r_isco = 0x0 ;
    p_f_isco = 0x0 ;
    p_lspec_isco = 0x0 ;
    p_espec_isco = 0x0 ;
    p_f_eq = 0x0 ;

}

void RotatingStarDiff::del_hydro_euler() {
    
    Star::del_hydro_euler() ;
        
    del_deriv() ;
        
}

    

  //--------------//
  //  Assignment  //
  //--------------//

  // Assignment to another Et_rot_diff
  // ---------------------------------
  void RotatingStarDiff::operator=(const RotatingStarDiff& et) {

    // Assignment of quantities common to all the derived classes of Etoile_rot
    Star::operator=(et) ;
    
    // Assignment of proper quantities of class Etoile_rot
      
    relativistic = et.relativistic ;    //  NEW
    unsurc2 = et.unsurc2 ;              //
      
    frot = et.frot ;                    //
    primfrot = et.primfrot ;            //
    par_frot = et.par_frot ;            //
    omega_field = et.omega_field ;      //      ORIGINAL
    prim_field = et.prim_field ;        //
    omega_min = et.omega_min ;          //
    omega_max = et.omega_max ;          //

    // All the other quantities
    a_car = et.a_car ;
    bbb = et.bbb ;
    b_car = et.b_car ;
    nphi = et.nphi ;
    tnphi = et.tnphi ;
    uuu = et.uuu ;
    nuf = et.nuf ;
    nuq = et.nuq ;
    dzeta = et.dzeta ;
    tggg = et.tggg ;
    w_shift = et.w_shift ;
    khi_shift = et.khi_shift ;
    tkij = et.tkij ;
    ak_car = et.ak_car ;
    ssjm1_nuf = et.ssjm1_nuf ;
    ssjm1_nuq = et.ssjm1_nuq ;
    ssjm1_dzeta = et.ssjm1_dzeta ;
    ssjm1_tggg = et.ssjm1_tggg ;
    ssjm1_khi = et.ssjm1_khi ;
    ssjm1_wshift = et.ssjm1_wshift ;
      
    del_deriv() ;  // Deletes all derived quantities
}

  //--------------//
  //  Outputs   //
  //--------------//

  // Save in a file
  // --------------

  void RotatingStarDiff::sauve(FILE* fich) const {
    
    Star::sauve(fich) ;
    par_frot.sauve(fich) ;
      
    fwrite(&relativistic, sizeof(bool), 1, fich) ;  // NEW
      
    omega_field.sauve(fich) ; 
    
    fwrite_be(&omega_min, sizeof(double), 1, fich) ;
    fwrite_be(&omega_max, sizeof(double), 1, fich) ;

    // NEW
    nuf.sauve(fich) ;
    nuq.sauve(fich) ;
    dzeta.sauve(fich) ;
    tggg.sauve(fich) ;
    w_shift.sauve(fich) ;
    khi_shift.sauve(fich) ;
    
    ssjm1_nuf.sauve(fich) ;
    ssjm1_nuq.sauve(fich) ;
    ssjm1_dzeta.sauve(fich) ;
    ssjm1_tggg.sauve(fich) ;
    ssjm1_khi.sauve(fich) ;
    ssjm1_wshift.sauve(fich) ;
  }


  // Printing
  // --------

  ostream& RotatingStarDiff::operator>>(ostream& ost) const {
    
    using namespace Unites ;

    Star::operator>>(ost) ;
    
    ost << endl ; 
    ost << "Differentially rotating  star" << endl ; 
    ost << "-----------------------------" << endl ;

    ost << endl << "Parameters of F(Omega) : " << endl ; 
    ost << par_frot << endl ; 
    
    ost << "Min, Max of Omega/(2pi) : " << omega_min / (2*M_PI) * f_unit 
	<< " Hz,  " << omega_max / (2*M_PI) * f_unit << " Hz" << endl ; 
    int lsurf = nzet - 1; 
    int nt = mp.get_mg()->get_nt(lsurf) ; 
    int nr = mp.get_mg()->get_nr(lsurf) ; 
    ost << "Central, equatorial value of Omega:        "
	<< omega_field.val_grid_point(0, 0, 0, 0) * f_unit << " rad/s,   "
	<< omega_field.val_grid_point(nzet-1, 0, nt-1, nr-1) * f_unit << " rad/s" << endl ;
    
    ost << "Central, equatorial value of Omega/(2 Pi): "
	<< omega_field.val_grid_point(0, 0, 0, 0) * f_unit / (2*M_PI) << " Hz,      "
	<< omega_field.val_grid_point(nzet-1, 0, nt-1, nr-1) * f_unit / (2*M_PI)
	<< " Hz" << endl ; 
    
    double nbar_max = max( max( nbar ) ) ;
    double ener_max = max( max( ener ) ) ;
    double press_max = max( max( press ) ) ;
    ost << "Max prop. bar. dens. :          " << nbar_max 
	<< " x 0.1 fm^-3 = " << nbar_max / nbar.val_grid_point(0, 0, 0, 0) << " central"
	<< endl ; 
    ost << "Max prop. ener. dens. (e_max) : " << ener_max
	<< " rho_nuc c^2 = " << ener_max / ener.val_grid_point(0, 0, 0, 0) << " central"
	<< endl ; 
    ost << "Max pressure :                  " << press_max
	<< " rho_nuc c^2 = " << press_max / press.val_grid_point(0, 0, 0, 0) << " central"
	<< endl ; 
   
    // Length scale set by the maximum energy density:
    double len_rho = 1. / sqrt( ggrav * ener_max ) ;
    ost << endl << "Value of A = par_frot(1) in units of e_max^{1/2} : " << 
      par_frot(1) / len_rho << endl ; 
    
    ost << "Value of A / r_eq : " << 
      par_frot(1) / ray_eq() << endl ; 
    
    ost << "r_p/r_eq : " << aplat() << endl ; 
    ost << "KEH l^2 = (c/G^2)^{2/3} J^2 e_max^{1/3} M_B^{-10/3} : " <<
      angu_mom() * angu_mom() / pow(len_rho, 0.6666666666666666)
      / pow(mass_b(), 3.3333333333333333) 
      / pow(ggrav, 1.3333333333333333) << endl ;
     
    ost << "M e_max^{1/2} : " << ggrav * mass_g() / len_rho << endl ; 
    
    ost << "r_eq e_max^{1/2} : " << ray_eq() / len_rho << endl ; 
    
    ost << "T/W : " << tsw() << endl ; 
    
    ost << "Omega_c / e_max^{1/2} : " << get_omega_c() * len_rho << endl ; 
     
    //display_poly(ost) ;

    return ost ;
    
    
  }

    
//from star_rot.C
void RotatingStarDiff::partial_display(ostream& ost) const {
    
    using namespace Unites ;
        
    double omega_c = get_omega_c() ;
    double freq = omega_c / (2.*M_PI) ;
    ost << "Central Omega : " << omega_c * f_unit
    << " rad/s     f : " << freq * f_unit << " Hz" << endl ;
    ost << "Rotation period : " << 1000. / (freq * f_unit) << " ms"
    << endl ;
    ost << endl << "Central enthalpy : " << ent.val_grid_point(0,0,0,0) << " c^2" << endl ;
    ost << "Central proper baryon density : " << nbar.val_grid_point(0,0,0,0)
    << " x 0.1 fm^-3" << endl ;
    ost << "Central proper energy density : " << ener.val_grid_point(0,0,0,0)
    << " rho_nuc c^2" << endl ;
    ost << "Central pressure : " << press.val_grid_point(0,0,0,0)
    << " rho_nuc c^2" << endl ;
        
    ost << "Central value of log(N)        : "
    << logn.val_grid_point(0, 0, 0, 0) << endl ;
    ost << "Central lapse N :      " << nn.val_grid_point(0,0,0,0) <<  endl ;
    ost << "Central value of dzeta=log(AN) : "
    << dzeta.val_grid_point(0, 0, 0, 0) << endl ;
    ost << "Central value of A^2 : " << a_car.val_grid_point(0,0,0,0) <<  endl ;
    ost << "Central value of B^2 : " << b_car.val_grid_point(0,0,0,0) <<  endl ;
        
    double nphi_c = nphi.val_grid_point(0, 0, 0, 0) ;
    if ( (omega_c==0) && (nphi_c==0) ) {
        ost << "Central N^phi :               " << nphi_c << endl ;
    }
    else{
        ost << "Central N^phi/Omega :         " << nphi_c / omega_c
        << endl ;
    }
    
    int lsurf = nzet - 1;
    int nt = mp.get_mg()->get_nt(lsurf) ;
    int nr = mp.get_mg()->get_nr(lsurf) ;
    ost << "Equatorial value of the velocity U:         "
    << uuu.val_grid_point(nzet-1, 0, nt-1, nr-1) << " c" << endl ;
    
    ost << endl
    << "Coordinate equatorial radius r_eq =    "
    << ray_eq()/km << " km" << endl ;
    ost << "Flattening r_pole/r_eq :        " << aplat() << endl ;
    
}
    
    
  // display_poly
  // ------------

  void RotatingStarDiff::display_poly(ostream& ost) const {

    using namespace Unites ;
    
    const Eos_poly* p_eos_poly = dynamic_cast<const Eos_poly*>( &eos ) ;   

    if (p_eos_poly != 0x0) {

      double kappa = p_eos_poly->get_kap() ; 
      double gamma = p_eos_poly->get_gam() ;  ; 

      // kappa^{n/2}
      double kap_ns2 = pow( kappa,  0.5 /(gamma-1) ) ; 
    
      // Polytropic unipoly ;
      // Polytropic unit of length in terms of r_unit :
      double r_poly = kap_ns2 / sqrt(ggrav) ;
        
      // Polytropic unit of time in terms of t_unit :
      double t_poly = r_poly ;
    
      // Polytropic unit of mass in terms of m_unit :
      double m_poly = r_poly / ggrav ;
    
      // Polytropic unit of angular momentum in terms of j_unit :
      double j_poly = r_poly * r_poly / ggrav ;

      // Polytropic unit of density in terms of rho_unit :
      double rho_poly = 1. / (ggrav * r_poly * r_poly) ;  

      ost.precision(10) ;
      ost << endl << "Quantities in polytropic units : " << endl ;
      ost << "  n_max     : " << max( max( nbar ) ) / rho_poly << endl ;
      ost << "  e_max     : " << max( max( ener ) ) / rho_poly << endl ;
      ost << "  P_min     : " << 2.*M_PI / omega_max / t_poly << endl ; 
      ost << "  P_max     : " << 2.*M_PI / omega_min / t_poly << endl ; 
      ost << "  P_c     : " << 2.*M_PI / get_omega_c() / t_poly << endl ;
      ost << "  M_bar   : " << mass_b() / m_poly << endl ;
      ost << "  M       : " << mass_g() / m_poly << endl ;
      ost << "  J	  : " << angu_mom() / j_poly << endl ;
      ost << "  r_eq	  : " << ray_eq() / r_poly << endl ;
      ost << "  R_circ  : " << r_circ() / r_poly << endl ;
      ost << "  R_mean  : " << mean_radius() / r_poly << endl ;
      
      int lsurf = nzet - 1;
      int nt = mp.get_mg()->get_nt(lsurf) ; 
      int nr = mp.get_mg()->get_nr(lsurf) ; 
      ost << "  P_eq      : " << 2.*M_PI / 
	omega_field.val_grid_point(nzet-1, 0, nt-1, nr-1) / t_poly << endl ;
          
    }

  }



  //-----------------------//
  //     Miscellaneous     //
  //-----------------------//

  double RotatingStarDiff::funct_omega(double omeg) const {

    return frot(omeg, par_frot) ;
    
  }

  double RotatingStarDiff::prim_funct_omega(double omeg) const {

    return primfrot(omeg, par_frot) ;
    
  }

  double RotatingStarDiff::get_omega_c() const {
    
    return omega_field.val_grid_point(0, 0, 0, 0) ; 
    
  }

  //-------------------------//
  //  Computational methods  //
  //-------------------------//
    
void RotatingStarDiff::fait_shift() {
    
    Vector d_khi = khi_shift.derive_con( mp.flat_met_cart() ) ;
        
    d_khi.dec_dzpuis(2) ;   // divide by r^2 in the external compactified domain
        
    // x_k dW^k/dx_i
    Scalar xx(mp) ;
    Scalar yy(mp) ;
    Scalar zz(mp) ;
    Scalar sintcosp(mp) ;
    Scalar sintsinp(mp) ;
    Scalar cost(mp) ;
    xx = mp.x ;
    yy = mp.y ;
    zz = mp.z ;
    sintcosp = mp.sint * mp.cosp ;
    sintsinp = mp.sint * mp.sinp ;
    cost = mp.cost ;
    
    int nz = mp.get_mg()->get_nzone() ;
    Vector xk(mp, COV, mp.get_bvect_cart()) ;
    xk.set(1) = xx ;
    xk.set(1).set_domain(nz-1) = sintcosp.domain(nz-1) ;
    xk.set(1).set_dzpuis(-1) ;
    xk.set(2) = yy ;
    xk.set(2).set_domain(nz-1) = sintsinp.domain(nz-1) ;
    xk.set(2).set_dzpuis(-1) ;
    xk.set(3) = zz ;
    xk.set(3).set_domain(nz-1) = cost.domain(nz-1) ;
    xk.set(3).set_dzpuis(-1) ;
    xk.std_spectral_base() ;
    
    Tensor d_w = w_shift.derive_con( mp.flat_met_cart() ) ;
    
    Vector x_d_w = contract(xk, 0, d_w, 0) ;
    x_d_w.dec_dzpuis() ;
    
    double lambda = double(1) / double(3) ;
    
    beta = - (lambda+2)/2./(lambda+1) * w_shift
    + (lambda/2./(lambda+1))  * (d_khi + x_d_w) ;
    
}
 
void RotatingStarDiff::fait_nphi() {
    
    if ( (beta(1).get_etat() == ETATZERO) && (beta(2).get_etat() == ETATZERO) ) {
        tnphi = 0 ;
        nphi = 0 ;
        return ;
    }
        
    // Computation of tnphi
    // --------------------
        
    mp.comp_p_from_cartesian( -beta(1), -beta(2), tnphi ) ;
        
    // Computation of nphi
    // -------------------
        
    nphi = tnphi ;
    nphi.div_rsint() ;

}

}
