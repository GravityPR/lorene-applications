/*
 * Reads binary neutron star-black hole initial data, exporting Lorene structures
 * onto standard C arrays (double[]) on a Cartesian grid.
 */

/*
 *   Copyright (c) 2001-2002  Eric Gourgoulhon
 *
 *   This file is part of LORENE.
 *
 *   LORENE is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2
 *   as published by the Free Software Foundation.
 *
 *   LORENE is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with LORENE; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

 

/*
 * $Id: readinit.C,v 1.9 2016/12/05 16:18:30 j_novak Exp $
 * $Log: readinit.C,v $
 * Revision 1.9  2016/12/05 16:18:30  j_novak
 * Suppression of some global variables (file names, loch, ...) to prevent redefinitions
 *
 * Revision 1.8  2014/10/13 08:54:04  j_novak
 * Lorene classes and functions now belong to the namespace Lorene.
 *
 * Revision 1.7  2014/10/06 15:09:46  j_novak
 * Modified #include directives to use c++ syntax.
 *
 * Revision 1.6  2003/01/09 11:07:59  j_novak
 * headcpp.h is now compliant with C++ norm.
 * The include files have been ordered, as well as the local_settings_linux
 *
 * Revision 1.5  2002/03/20 08:24:56  e_gourgoulhon
 * Added the derivatives of Psi.
 *
 * Revision 1.4  2002/02/06 14:54:44  e_gourgoulhon
 * Update of bibliographical references
 *
 * Revision 1.3  2002/01/09 16:32:57  e_gourgoulhon
 * Added the parameter fill in readinit.par
 * Suppressed the multiple outputs in files ini* via method sauve
 * Output directly written in readinit.C
 *
 * Revision 1.2  2001/12/18 09:08:14  e_gourgoulhon
 * Adds the filling of the holes interiors
 *
 * Revision 1.1  2001/12/14 08:59:18  e_gourgoulhon
 * Exportation of Lorene Bhole_binaire object to a Cartesian grid
 *
 *
 * $Header: /cvsroot/Lorene/Export/BinBH/readinit.C,v 1.9 2016/12/05 16:18:30 j_novak Exp $
 *
 */

// C headers
#define H5_USE_18_API
#define H5Acreate_vers 2
#define H5Dcreate_vers 2
#define H5Dopen_vers 2
#include <cstdlib>
#include <hdf5.h>

// Definition of Bin_BH class
#include "bin_ns_bh_export.h"
#include "unites.h"
using namespace Lorene ;

double const CU_to_km = 1.4765813442824236;
double const CU_to_s  = 4.925345200920376e-06;
double const coord_unit = CU_to_km ; 

int main() {
    
    cout << "Reading from file readinit.par" << endl;

    // Reads Cartesian grid parameters
    // -------------------------------
    ifstream fparam("readinit.par") ; 
    char comment[120] ; 
    char datafile[120] ;
    int nx, ny, nz, fill ;
    double x_min, x_max, y_min, y_max, z_min, z_max ;
    fparam.getline(comment, 120) ;
    fparam.getline(comment, 120) ;
    fparam.getline(datafile, 120) ;
    fparam.getline(comment, 120) ;
    fparam.getline(comment, 120) ; 
    fparam.getline(comment, 120) ;
    fparam >> fill ; fparam.getline(comment, 120) ;
    fparam >> nx ; fparam.getline(comment, 120) ;
    fparam >> ny ; fparam.getline(comment, 120) ;
    fparam >> nz ; fparam.getline(comment, 120) ;
    fparam >> x_min ; fparam >> x_max ; fparam.getline(comment, 120) ; 
    fparam >> y_min ; fparam >> y_max ; fparam.getline(comment, 120) ; 
    fparam >> z_min ; fparam >> z_max ; fparam.getline(comment, 120) ; 
    fparam.close() ; 
    
    cout << "File containing the initial data on the spectral grid:"
	 << endl ; 
    cout << datafile << endl << endl ; 
    cout << "Cartesian grid : " << endl ; 
    cout << "---------------- " << endl ; 
    cout << "   Number of points in the x direction : " << nx << endl ; 
    cout << "   Number of points in the y direction : " << ny << endl ; 
    cout << "   Number of points in the z direction : " << nz << endl ; 
    cout << "   x_min, x_max : " << x_min << " , " << x_max << endl ; 
    cout << "   y_min, y_max : " << y_min << " , " << y_max << endl ; 
    cout << "   z_min, z_max : " << z_min << " , " << z_max << endl ; 
    
    double* xx ;     /// 1-D array storing the values of coordinate y of the {\tt np} grid points [unit: km]
    double* yy ;     /// 1-D array storing the values of coordinate z of the {\tt np} grid points [unit: km]
    double* zz ;     /// Lapse function $N$ at the {\tt np} grid points (1-D array)
    double* nnn ;    /// Component $\beta^x$ of the shift vector of non rotating coordinates [unit: $c$]
    double* beta_x ; /// Component $\beta^y$ of the shift vector of non rotating coordinates [unit: $c$]
    double* beta_y ; /// Component $\beta^z$ of the shift vector of non rotating coordinates [unit: $c$]
    double* beta_z ; /// Metric coefficient $\gamma_{xx}$ at the grid points (1-D array)
    double* g_xx ;   /// Metric coefficient $\gamma_{xy}$ at the grid points (1-D array)
    double* g_xy ;   /// Metric coefficient $\gamma_{xz}$ at the grid points (1-D array)
    double* g_xz ;   /// Metric coefficient $\gamma_{yy}$ at the grid points (1-D array)
    double* g_yy ;   /// Metric coefficient $\gamma_{yz}$ at the grid points (1-D array)
    double* g_yz ;   /// Metric coefficient $\gamma_{zz}$ at the grid points (1-D array)
    double* g_zz ;   /// Component $K_{xx}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
    double* k_xx ;   /// Component $K_{xy}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
    double* k_xy ;   /// Component $K_{xz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
    double* k_xz ;   /// Component $K_{yy}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
    double* k_yy ;   /// Component $K_{yz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
    double* k_yz ;   /// Component $K_{zz}$ of the extrinsic curvature at the grid points (1-D array) [unit: c/km]
    double* k_zz ;
    double* nbar ;
    double* ener_spec ;
    double* u_euler_x ;
    double* u_euler_y ;
    double* u_euler_z ;
    
    char file_out[128]  = "results.h5";

    
    
    // Construction of the Cartesian grid with correct spacings and boundaries (LORENE's)
    // -----------------------------------------------------------------------------------
    
    int nbp = nx*ny*nz ; 
    double* const xi = new double[nbp] ; 
    double* const yi = new double[nbp] ; 
    double* const zi = new double[nbp] ;

    // LORENE's rescaled grid
    double* const xi_L = new double[nbp] ; 
    double* const yi_L = new double[nbp] ; 
    double* const zi_L = new double[nbp] ;

    
    double dx = (nx == 1) ? 0 : (x_max - x_min) / double(nx - 1) ;
    double dy = (ny == 1) ? 0 : (y_max - y_min) / double(ny - 1) ;
    double dz = (nz == 1) ? 0 : (z_max - z_min) / double(nz - 1) ;

    double* pxi = xi ;
    double* pyi = yi ;
    double* pzi = zi ;
    
    // LORENE's rescaled Cartesian Grid
    double* pxi_L = xi_L ;
    double* pyi_L = yi_L ;
    double* pzi_L = zi_L ;

    for (int k=0; k<nz; k++) {

	double z = z_min + dz * k ;

	for (int j=0; j<ny; j++) {

	    double y = y_min + dy * j ;

	    for (int i=0; i<nx; i++) {

		*pxi = x_min + dx * i ;  
		*pyi = y ; 
		*pzi = z ; 
		*pxi_L = (x_min + dx * i) * coord_unit ;  
		*pyi_L = y * coord_unit ; 
		*pzi_L = z * coord_unit ; 
		
		
		pxi++ ; 
		pyi++ ; 
		pzi++ ;

		pxi_L++;
		pyi_L++;
		pzi_L++;
	    }
	}
    }
    
    //xx = pxi ;
    //yy = pyi ;
    //zz = pzi ;
    cout << endl << "LORENE's Cartesian grid:" << endl;
    cout << "-------------------------" << endl;
    cout << "   x_min, x_max : " << x_min  * CU_to_km  << " , " << x_max * CU_to_km  << endl ;
    cout << "   y_min, y_max : " << y_min  * CU_to_km  << " , " << y_max * CU_to_km  << endl ;
    cout << "   z_min, z_max : " << z_min  * CU_to_km  << " , " << z_max * CU_to_km  << endl ;
    cout << "   x_L[0], x_L[end] : " << xi_L[0] << "  " << xi_L[nx-1] << endl ;
    cout << "   x[0], x[end] : " << xi[0] << "  " << xi[nx-1] << endl ;
    
    xx = new double[nbp];
    yy = new double[nbp];
    zz = new double[nbp];
    
    nnn = new double[nbp] ;
    
    beta_x = new double[nbp] ;
    beta_y = new double[nbp] ;
    beta_z = new double[nbp] ;
    
    g_xx = new double[nbp] ;
    g_xy = new double[nbp] ;
    g_xz = new double[nbp] ;
    g_yy = new double[nbp] ;
    g_yz = new double[nbp] ;
    g_zz = new double[nbp] ;

    k_xx = new double[nbp] ;
    k_xy = new double[nbp] ;
    k_xz = new double[nbp] ;
    k_yy = new double[nbp] ;
    k_yz = new double[nbp] ;
    k_zz = new double[nbp] ;

    nbar = new double[nbp] ;

    ener_spec = new double[nbp] ;

    u_euler_x = new double[nbp] ;
    u_euler_y = new double[nbp] ;
    u_euler_z = new double[nbp] ;
    
    Bin_NS_BH_export binary(nbp, xi_L, yi_L, zi_L, fill, datafile) ;
    
    cout << binary << endl ;
    
    for (int i=0; i<nbp; i++)  {

        xx[i]  = xi[i];
        yy[i]  = yi[i];
        zz[i]  = zi[i];
        
        nnn[i] = binary.nnn[i];
        
        beta_x[i] = binary.beta_x[i];
        beta_y[i] = binary.beta_y[i];
        beta_z[i] = binary.beta_z[i];
        
        g_xx[i] = binary.g_xx[i];
        g_xy[i] = binary.g_xy[i];
        g_xz[i] = binary.g_xz[i];
        g_yy[i] = binary.g_yy[i];
        g_yz[i] = binary.g_yz[i];
        g_zz[i] = binary.g_zz[i];
        
        k_xx[i] = binary.k_xx[i] * coord_unit;
        k_xy[i] = binary.k_xy[i] * coord_unit;
        k_xz[i] = binary.k_xz[i] * coord_unit;
        k_yy[i] = binary.k_yy[i] * coord_unit;
        k_yz[i] = binary.k_yz[i] * coord_unit;
        k_zz[i] = binary.k_zz[i] * coord_unit;

	// Hydro stuff!

	nbar[i] = binary.nbar[i] / Unites::rho_unit ;

	ener_spec[i] = binary.ener_spec[i] ; // Don't remember the unit RN, will make this right
	
	u_euler_x[i] = binary.u_euler_x[i] ;
	u_euler_y[i] = binary.u_euler_y[i] ;
	u_euler_z[i] = binary.u_euler_z[i] ;
					    
        
    }
    // ------------------------------------------
    // HDF5 save
    // ------------------------------------------
    
    hid_t       file_id;   /* file identifier */
    herr_t      status;
    
    hid_t       attribute_id; /* identifiers for attributes*/
    hid_t       attributeH5type,attributeH5c128;
    
    hid_t       dataset_id, dataspace_id;  /* identifiers for dsets*/
    hsize_t     dims[3];
    
    int         i,j;
    double      *dset_data;
    double      **var;
    int         varIndex;
    
    cout << "I am here " << endl;

    /* =========================================== */
    /* Create a new file using default properties. */
    /* =========================================== */
    file_id = H5Fcreate(file_out, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    
    dims[0] = nx;
    dims[1] = ny;
    dims[2] = nz;
    
    double* varlist[]  = {xx,yy,zz,
			  nnn,beta_x,beta_y,beta_z,
			  g_xx,g_xy,g_xz,g_yy,g_yz,g_zz,
			  k_xx,k_xy,k_xz,k_yy,k_yz,k_zz,
			  u_euler_x,u_euler_y,u_euler_z,nbar,ener_spec
    };
    char  * varnames[] = {"/X[0]","/X[1]","/X[2]",
			  "/alp","/betax","/betay","/betaz",
			  "/gxx","/gxy","/gxz","/gyy","/gyz","/gzz",
			  "/Kxx","/Kxy","/Kxz","/Kyy","/Kyz","/Kzz",
			  "/u_x","/u_y","/u_z","/rho","/e" 
    };
    
    for(int idx=0;idx< 24; idx++) {
        dataspace_id = H5Screate_simple(3, dims, NULL);
        dataset_id = H5Dcreate(file_id, varnames[idx],
                               H5T_NATIVE_DOUBLE, dataspace_id,
                               H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        status = H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
                          varlist[idx]);
        status = H5Sclose(dataspace_id);
        status = H5Dclose(dataset_id);
    }
    
    status = H5Fclose(file_id);
    // ------------------------------------------
    // Deallocate variables
    // ------------------------------------------
    delete [] xx ;
    delete [] yy ;
    delete [] zz ;
    
    delete [] nnn ;
    
    delete [] beta_x ;
    delete [] beta_y ;
    delete [] beta_z ;
    
    delete [] g_xx ;
    delete [] g_xy ;
    delete [] g_xz ;
    delete [] g_yy ;
    delete [] g_yz ;
    delete [] g_zz ;
    
    delete [] k_xx ;
    delete [] k_xy ;
    delete [] k_xz ;
    delete [] k_yy ;
    delete [] k_yz ;
    delete [] k_zz ;

    delete [] nbar ;

    delete [] ener_spec ;

    delete [] u_euler_x ;
    delete [] u_euler_y ;
    delete [] u_euler_z ; 
/*
    // Read of the initial data file and computation of the
    //  fields at the Cartesian grid points
    // the 1 at the end of binary means that it's an unequal mass model
    // ----------------------------------------------------

    Bin_BH binary(nbp, xi, yi, zi, fill, datafile, 1) ;

    cout << binary << endl ;

    ofstream fich("readout.d") ;

    fich.precision(13) ;
    fich.setf(ios::scientific, ios::floatfield) ;

    fich << binary.dist << " dist [a]" << endl ;
    fich << "nx, ny, nz : " << nx << "  " << ny << "  " << nz << endl ;
    fich << "x_min, dx : " << x_min << "  " << dx << endl ;
    fich << "y_min, dy : " << y_min << "  " << dy << endl ;
    fich << "z_min, dz : " << z_min << "  " << dz << endl ;

    int index = 0 ;

    for (int k=0; k<nz; k++) {

	for (int j=0; j<ny; j++) {

	    for (int i=0; i<nx; i++) {

                fich << i
                  << " " <<  binary.g_xx[index] << " " <<  binary.g_xy[index] << " " <<  binary.g_xz[index]
                  << " " <<  binary.g_yy[index] << " " <<  binary.g_yz[index] << " " <<  binary.g_zz[index]
                  << " " <<  binary.k_xx[index] << " " <<  binary.k_xy[index] << " " <<  binary.k_xz[index]
                  << " " <<  binary.k_yy[index] << " " <<  binary.k_yz[index] << " " <<  binary.k_zz[index]
                  << " " <<  binary.beta_x[index] << " " <<  binary.beta_y[index]
                  << " " <<  binary.beta_z[index]  << " " <<  binary.nnn[index] << endl ;

                index++ ;

                        }
                }

        }


    fich.close() ;



    // Save in a binary file
    // ---------------------
    FILE* finib = fopen("binbh.d", "w") ;
    binary.save_bin(finib) ;
    fclose( finib ) ;

    // Save in a formatted file
    // ------------------------
    // ofstream finif("inif.d") ;
    // binary.save_form(finif) ;
    // finif.close() ;


    // Clean exit
    // ----------

    delete [] xi ;
    delete [] yi ;
    delete [] zi ;

    return EXIT_SUCCESS ;
*/
}
