#!/usr/bin/env python

##!/hpc/group/G_NUMREL/python_envs/data_analysis2/bin/python

##!/Users/carlomusolino/pyEnvs/data_processing2/bin/python

import os
import sys
import re
import getopt 
import numpy as np

try :
    INIT_BIN =  os.environ['INIT_BIN']
    COAL     =  os.environ['COAL']
except :
    # COAL     = '/Users/depietri/EinsteinToolkit/ET_2016_11/CactusPR/exe/PR/Meudon_Bin_NS_coal'
    # INIT_BIN = '/Users/depietri/EinsteinToolkit/ET_2016_11/CactusPR/exe/PR/Meudon_Bin_NS_init_bin'
    # COAL     = '/Users/depietri/SVN/BNSdisk/VERSION_1_BNS_ID/InitialModels/Meudon_Bin_NS_coal'
    # INIT_BIN = '/Users/depietri/SVN/BNSdisk/VERSION_1_BNS_ID/InitialModels/Meudon_Bin_NS_init_bin'
#    COAL     = '/Users/depietri/SVN/BNSdisk/VERSION_2_BNS_ID/InitialModels/coal'
#    INIT_BIN = '/Users/depietri/SVN/BNSdisk/VERSION_2_BNS_ID/InitialModels/init_bin'
#    COAL      = "/Users/carlomusolino/thesiscode/Lorene/Codes/Bin_ns_bh/coal_ns_bh"
#    INIT_NS_BH      = "/Users/carlomusolino/thesiscode/Lorene/Codes/Bin_ns_bh/init_ns_bh"
#    BASEPATH    = "/hpc/group/G_NUMREL/EinsteinToolkit/lorene/Codes/Bin_ns_bh"
#    EOSPATH     = "/hpc/group/G_NUMREL/EOS_tables"
#    COAL        = BASEPATH+"/coal_ns_bh"
#    INIT_NS_BH  = BASEPATH+"/init_ns_bh"
    BASEPATH    = "/Users/michele/BitbucketProjects/lorene-applications/Applications/Bin_NS_BH"
    EOSPATH     = "/Users/michele/BitbucketProjects/lorene-applications/Applications/Bin_NS_BH/Script/Eos_tables"
    COAL        = BASEPATH+"/Coal_NS_BH"
    INIT_NS_BH    = BASEPATH+"/Init_NS_BH"
    
print("GenerateLorenModel: Generation of Lorene Model for binay Black Hole-Neutron Star")
print(" Options: EOStable   [To be locate in ../Eos_tables/]")
print("          M_bh         [ADM Mass of the Black Hole]")
print("          M_ns         [Barionic mass of the Star]")
print("          d          [Coordinate distance of the centres]")
print("")
print(" It will use the PROGRAM: [$INIT_NS_BH] : " ,INIT_NS_BH)
print("                          [$COAL]     : " ,COAL)


##############################################################################################
##
##  GIVEN vars['name'] = val REPLACE IN str THE VALUE  ${var} => val
##
##############################################################################################

def REPLACE(str,vars) :
    for v in list(vars):
        str = re.sub('\${' + v +'}',vars[v],str)
    return str

##############################################################################################
##
##  MAIN LORENE SETTING 
##
##############################################################################################
LORENE = dict()
###################################################################################
LORENE['par_eos'] = """17 Type of the EOS (cf. documentation of Eos::eos_from_file)
0  Standard Lorene format
Tabulated EOS
../../Eos_tables/${EOS}.lorene
"""
###################################################################################

LORENE['par_eos'] = """17 Type of the EOS (cf. documentation of Eos::eos_from_file)
0  Standard Lorene format
1  Enforce thermo consistency Tabulated EOS
"""+EOSPATH+"""/${EOS}.lorene
"""

# Even number of angular points --> impose symmetry 

LORENE['par_grid_ns'] = """# Multi-grid parameters for the NS
##########################################
8    nz: total number of domains
1
21    nt: number of points in theta (the same in each domain)
20    np: number of points in phi   (the same in each domain)
# Number of points in r and (initial) inner boundary of each domain:
33      0.    <-   nr      &   min(r)  in domain 0  (nucleus)
33      1.5
33      3.     <-   nr   &   min(r)  in domain 2
33      6.
33      12
33      24
33      48
33      96
"""

LORENE['par_grid_bh'] = """# Multi-grid parameters for the black hole
##########################################
9    nz: total number of domains
21    nt: number of points in theta (the same in each domain)
20    np: number of points in phi   (the same in each domain)
# Number of points in r and (initial) inner boundary of each domain:
33      0.    <-   nr      &   min(r)  in domain 0  (nucleus
33      0.5
33      1
33      2
33      4
33      8
33      16
33      32
33      64
"""

LORENE['par_init'] = """# Physical parameters for the binary initial conditions
#######################################################
600.    <- coordinate distance between the two centers [km]
0.238  <- initial central enthalpy of the star
1    <- rotational state of the star  : 1 = irrotational, 0 = corotating
1       <- rotational state of the black hole : 1 = irrotational, 0 = corotating
"""

LORENE['par_coal'] = """
${d}            #Coordinate distance
${M_bh} ${M_ns} #Masses BH NS (in solar mass)
1.e-3           #Treshold to converge to the masses
1.e-7           #Precision
0.6             #Relaxation
1               #Max Poisson
4               #Autre truc Poisson
0.              #Spin
0.2             #Scale Spin
0.1             #Lapse Horizon
"""
##############################################################################################
##
##
##############################################################################################


def GenerateModel(EOS,M_bh=5,M_ns=1.4,distance=50.0,ModelName='Test',PATH='..',UseGravitationalMass=False) :

    ModelName2 = (  EOS + '_' +  ("%03d" % int(M_bh*100)) + 'vs' + ("%03d" % int(M_ns*100)) 
              + '_d' + ("%d" % int(distance)) ) 
    print("Genereting MEUDON data for Model: ",ModelName," -- ", ModelName2) 
    vars= {
       'EOS' : EOS ,
       'd'   : str(float(distance)/100.0),
       'M_bh'  : str(float(M_bh)),
       'M_ns'  : str(float(M_ns))
    }
    for i in range(1000):
        TEMP_DIR_MAME = 'temp'+ ('%03d'% i)
        #print "Checking ", TEMP_DIR_MAME 
        if  (not os.path.exists(TEMP_DIR_MAME)) :
            #print "Do not exist: ", TEMP_DIR_MAME 
            break
    os.mkdir(TEMP_DIR_MAME)
    os.chdir(TEMP_DIR_MAME)
    for i in list(LORENE) :
         f = open(i+'.d', 'w')
         f.write(REPLACE(LORENE[i],vars))
         f.close()
    print("Starting Initialization... ")
    os.system(INIT_NS_BH+ " | tee init.out")
    print("Init done. Starting coal...")
    os.system(COAL + " par_coal.d" + " statiques.dat" + " | tee coal.out")
    print("Coal done.")
    os.system('touch resu.d')
    os.system('tar czvf ' + ModelName + '.tgz *')
    if not os.path.exists(os.path.join(PATH,'Models')):
      os.makedirs(os.path.join(PATH,'Models'))
    os.rename(ModelName + '.tgz',os.path.join(PATH,'Models',ModelName + '.tgz'))
    os.rename('resu.d',os.path.join(PATH,'Models',ModelName + '.resu_d'))
    os.chdir('..')



#    vars= {
#       'EOS' : 'SLy_PP' ,
#       'd'   : str(40.0/100)
#    }

## print "./GenerateLoreneModel.py -1 1.4  -2 1.4 -d 50 -e PP_Read_SLy -n SLy_d50"


def Get_BarionMass_outof_GravitationalMass(TOVtable,Mg) :
    rhoc,ec,rp_re,A_diff,Re,M0,M,W,T,J,beta,M_R,invPa,invPe,invtd=np.loadtxt(TOVtable,unpack=True)
    Mb =np.interp(Mg, M, M0)
    return Mb


######################################################
###  FROM COMMAND LINE
######################################################

if __name__ == "__main__":

    PATH = os.path.realpath('.')
    EOS = 'PP_SFHo'
    M_bh  = 5
    M_ns  = 1.5
    d   = 50
    ModelName = 'Test'
    TOVtable = ''

    usage = "usage: "+ sys.argv[0]+" -e EOSname -t TOVtable -b M_bh -s M_ns -d distance"
    #################################
    # PARSE command line arguments
    #################################
    try:
        opts, args = getopt.getopt(sys.argv[1:],"ht:e:b:s:d:n:")
    except :
        print(usage)
        sys.exit(2)

    for opt, arg in opts:
        if ( opt == '-h') :
            print(usage)
            sys.exit(2)
        elif (opt == '-d') :
            d = float(arg)/10.0
        elif (opt == '-b') :
            M_bh = float(arg)
        elif (opt == '-s') :
            M_ns = float(arg)
        elif (opt == '-e') :
            EOS = (arg)
        elif (opt == '-n') :
            ModelName = (arg)
        elif (opt == '-t') :
            TOVtable = (arg)

    if (len(TOVtable)>  0 ) :
        print( "Selected to use Gravitational Mass TOV table="+TOVtable)
        MG_ns = M_ns
        M_ns = Get_BarionMass_outof_GravitationalMass(TOVtable,MG_ns)
        print("The NS has M_grav="+str(MG_ns)+" and M_baryon="+str(M_ns))

    ####################################
    # END PARSE command line arguments
    ####################################

    GenerateModel(EOS,M_bh,M_ns,d,ModelName,PATH=PATH) 

 
