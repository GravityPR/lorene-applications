# Physical parameters for the binary initial conditions
#######################################################
600.	<- coordinate distance between the two centers [km]
0.238  <- initial central enthalpy of the star
1	<- rotational state of the star  : 1 = irrotational, 0 = corotating
1       <- rotational state of the black hole : 1 = irrotational, 0 = corotating
