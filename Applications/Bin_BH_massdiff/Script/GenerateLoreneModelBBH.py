#!/hpc/group/G_NUMREL/python_envs/data_analysis2/bin/python


import os
import sys
import re
import getopt 
import numpy as np

try :
    INIT_BIN =  os.environ['INIT_BIN']
    COAL     =  os.environ['COAL']
except :
    BASEPATH    = "/hpc/group/G_NUMREL/EinsteinToolkit/lorene/Codes/Bin_BH_mass_diff"
    COAL        = BASEPATH+"/coal_bh_mdiff"
    INIT_BH_MDIFF  = BASEPATH+"/init_bh_mdiff"

print("GenerateLorenModel: Generation of Lorene Model for binay Black Holes")
print(" Options:                                                 ")
print("          M_2         [Requested irreducible mass of BH 1]")
print("          M_1         [Requested irreducible mass of BH 2]")
print("          d          [Coordinate distance of the centres]")
print("")
print(" It will use the PROGRAM: [$INIT_BH_MDIFF] : " ,INIT_BH_MDIFF)
print("                          [$COAL]     : " ,COAL)


##############################################################################################
##
##  GIVEN vars['name'] = val REPLACE IN str THE VALUE  ${var} => val
##
##############################################################################################

def REPLACE(str,vars) :
    for v in list(vars):
        str = re.sub('\${' + v +'}',vars[v],str)
    return str

##############################################################################################
##
##  MAIN LORENE SETTING 
##
##############################################################################################
LORENE = dict()
###################################################################################

# Even number of angular points --> impose symmetry 

LORENE['param_statiques'] = """
${d}        distance
${n1} ${n2}        nzones dans le deux domains
${nr} ${nt} ${nphi}    points in r theta and phi 
${g1}       boundaries of the domains for BH 1
${g2}       boundaries of the domains for BH 2
1e-7        convergence threshold
0.5         relaxation
"""

LORENE['param_coal'] = """
${M_1} ${M_2}  Les masses vers lesquelles on veut converger...
1e-3          search omega threshold
1e-7           convergence threshold
0.5            relaxation
10             number of steps to go from 0 to "real" omega
"""
##############################################################################################


def GenerateModel(M_1=2.5,M_2=25,distance=100,ModelName='Test',PATH='..',dbg=False):
    ModelName2 = (  ("%02d" % int(M_1*100)) + 'vs' + ("%02d" % int(M_2*100)) 
              + '_d' + ("%d" % int(distance)) ) 
    print("Genereting MEUDON data for Model: ",ModelName," -- ", ModelName2) 
    n1 = 9
    g1 = "0 "
    default_bounds = np.array([ 1, 2, 4, 8, 16, 32, 64, 128])
    # Compute the correct boundaries for first BH, a radius of 1 works fine for a 2.5 M_sol BH
    new_bounds = default_bounds * float(M_1)/2.5 
    if new_bounds[-1] < 128: new_bounds[-1] *= 2 
    for b in new_bounds:
        g1 += "%3.5f "%b

    # Compute the correct boundaries for second BH depending on mass ratio
    
    g2 = "0 "
    default_bounds = np.array([10.0, 16.0, 32.0, 64.0, 98.0, 99.0, 100.0, 101.0, 128.0])
    Q = float(M_2) / M_1
    M = new_bounds[-1] - Q

    print("Computing model for mass ratio:  %2.1f" %Q)

    if int(Q)==1:
        n2 = n1
    else:
        n2 = 10
        new_bounds = M/(default_bounds[-1]-default_bounds[0]) * (default_bounds - default_bounds[0]) + Q
    for b in new_bounds:
        g2 +=  "%3.5f "%b
    
    print("Grid spacings for BH2:               ")
    print(g2)

    nr = 33; nt = 21; nphi = 20
    if dbg: nr = 17; nt = 13; nphi = 12
    
    vars= {
       'd'   : str(float(distance)),
        'M_1'  : str(float(M_1)),
        'M_2'  : str(float(M_2)),
        "g1"   : g1,
        "g2"   : g2,
        "n1"   : str(n1),
        "n2"   : str(n2),
        "nr"   : str(nr),
        "nt"   : str(nt),
        "nphi"   : str(nphi)
    }
    # Create a temporary directory to hold intermediate files
    for i in range(1000):
        TEMP_DIR_MAME = 'temp'+ ('%03d'% i)
        #print "Checking ", TEMP_DIR_MAME
        if  (not os.path.exists(TEMP_DIR_MAME)) :
            #print "Do not exist: ", TEMP_DIR_MAME
            break
    os.mkdir(TEMP_DIR_MAME)
    os.chdir(TEMP_DIR_MAME)
    for i in list(LORENE) :
         f = open(i+'.d', 'w')
         f.write(REPLACE(LORENE[i],vars))
         f.close()
    # Compute model
    print("Starting Initialization... ")
    os.system(INIT_BH_MDIFF+" param_statiques.d" +" | tee init.out")
    print("Init done. Starting coal...")
    os.system(COAL + " param_coal.d" + " statiques.dat" + " | tee coal.out")
    print("Coal done.")
    # Save model
    os.system("cp init.out "+ModelName2+".out")
    os.system('touch resu.d')
    os.system('tar czvf ' + ModelName + '.tgz *')
    if not os.path.exists(os.path.join(PATH,'Models')):
      os.makedirs(os.path.join(PATH,'Models'))
    os.rename(ModelName + '.tgz',os.path.join(PATH,'Models',ModelName + '.tgz'))
    os.rename('resu.d',os.path.join(PATH,'Models',ModelName + '.resu_d'))
    os.chdir('..')
    return

######################################################
###  FROM COMMAND LINE
######################################################

if __name__ == "__main__":

    PATH = os.path.realpath('.')
    M_1  = 2.5
    M_2  = 25
    d   = 100
    ModelName = 'Test'
    TOVtable = ''
    debug = False

    usage = "usage: "+ sys.argv[0]+" -1 M_1 -2 M_2 -d distance[km] -n ModelName-o optimize[1 for true 0 for false]"
    #################################
    # PARSE command line arguments
    #################################
    try:
        opts, args = getopt.getopt(sys.argv[1:],"ht:e:1:2:d:n:")
    except :
        print(usage)
        sys.exit(2)

    for opt, arg in opts:
        if ( opt == '-h') :
            print(usage)
            sys.exit(2)
        elif (opt == '-d') :
            d = float(arg)
        elif (opt == '-1') :
            M_1 = float(arg)
        elif (opt == '-2') :
            M_2 = float(arg)
        elif (opt == '-n') :
            ModelName = (arg)
        elif (opt == "-o") :
            debug = not bool(arg)
        

    ####################################
    # END PARSE command line arguments
    ####################################

    GenerateModel(M_1,M_2,d,ModelName,PATH=PATH,dbg=debug) 

###################################
# END of GenerateLoreneModelsBBH
###################################
